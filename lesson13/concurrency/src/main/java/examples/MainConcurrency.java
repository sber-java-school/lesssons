package examples;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class MainConcurrency {

    public static final Object o1 = new Object();
    public static final Object o2 = new Object();
    public static final SberCounter COUNTER = new AtomicCounter(0);
//    public static final SberCounter COUNTER = new SimpleCounter(0);

    public static void main(String[] args) throws ClassNotFoundException, InterruptedException, ExecutionException {
        counterExample();
        synchronizedCollectionExample();
        executorServiceExample();
    }

    private static void counterExample() {
        Random random = new Random();
        Lock lock = new ReentrantLock();

        Runnable runnable = () -> {
            for (int i = 0; i < 100; i++) {
                while (!(lock.tryLock())) {
                    System.out.printf("%s try lock...\n", Thread.currentThread().getName());
                }

                try {
                    final int one = COUNTER.incAndGet();
                    try {
                        TimeUnit.MILLISECONDS.sleep(random.nextLong(10));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    final int zero = COUNTER.decAndGet();
                    if (one != 1 || zero != 0) {
                        System.out.printf("Thread %s: one = %s, zero = %s\n", Thread.currentThread().getName(), one, zero);
                    }
                } finally {
                    lock.unlock();
                }
            }
        };

        for (int i = 0; i < 10; i++) {
            new Thread(runnable).start();
        }
    }

    private static void executorServiceExample() throws InterruptedException, ExecutionException {
        ExecutorService cachedThreadPool = Executors.newCachedThreadPool();

        Future<Integer> firstTask = cachedThreadPool.submit(() -> {
            var result = 0;
            for (int i = 0; i < 10; i++) {
                result += COUNTER.incAndGet();
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    System.out.printf("%s isInterrupted: %s \n", Thread.currentThread().getName(), Thread.currentThread().isInterrupted());
                    Thread.currentThread().interrupt();
                    e.printStackTrace();
                    return -1;
                }
            }
            return result;
        });

        while (!firstTask.isDone()) {
            System.out.println("FirstTask is not completed");
            TimeUnit.MILLISECONDS.sleep(10);
            //firstTask.cancel(true);
        }

        if (firstTask.isDone() && !firstTask.isCancelled()) {
            Integer result = firstTask.get();
            System.out.printf("FirstTask was completed: %s", result);
        }

        cachedThreadPool.shutdown();
    }

    private static void synchronizedCollectionExample() {
        Map hashMap = new HashMap();

        Map map = Collections.synchronizedMap(hashMap);
    }


    interface SberCounter {
        int incAndGet();

        int decAndGet();
    }

    static class AtomicCounter implements SberCounter {
        private final AtomicInteger atomicInteger;

        public AtomicCounter(int i) {
            this.atomicInteger = new AtomicInteger(i);
        }

        public int incAndGet() {
            return atomicInteger.incrementAndGet();
        }

        public int decAndGet() {
            return atomicInteger.decrementAndGet();
        }
    }

    static class SimpleCounter implements SberCounter {
        private int i;

        public SimpleCounter(int i) {
            this.i = i;
        }

        public int incAndGet() {
            return ++i;
        }

        public int decAndGet() {
            return --i;
        }
    }
}

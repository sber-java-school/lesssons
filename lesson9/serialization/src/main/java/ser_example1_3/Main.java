package ser_example1_3;

import java.io.*;

/**
 * ПРимеры выбрасывания исключений
 */
public class Main {

    //Генерим ClassNotFoundException - переименовываем проект 1_2
    //Генерим InvalidClassException  - меняем номер версии класса Клиент
    public static void main(String[] args) throws IOException, ClassNotFoundException  {
        try {
            System.out.println("Start deserialization");
            FileInputStream fis = new FileInputStream("serial_personFile2.bin");
            ObjectInputStream ios = new ObjectInputStream(fis);

            System.out.println(ios.readObject());
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

}

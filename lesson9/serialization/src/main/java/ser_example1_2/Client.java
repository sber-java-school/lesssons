package ser_example1_2;

import java.io.Serializable;

public class Client extends Person implements Serializable {

    private static final long serialVersionUID = 6628232698305516464L;

    private long id;

    public Client(long id, String name, int age) {
        super(name, age);

        this.id = id;
        System.out.println("call Client(long id, String name, int age) constructor");
    }

    public long getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Client{" +
                "id=" + id +
                ", name='" + getName() + '\'' +
                ", age=" + getAge() +
                '}';
    }
}

package ser_example1_2;

import java.io.*;

/**
 * Person не расширяет Serializable. Для сериализации дочерних классов Person требуется дефолтный конструктор в классе Person.
 * При наличии дефолтного конструктора в классе Person сериалзиация выполняется, но все его поля имеют значения по умолчанию.
 */
public class Main {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        FileOutputStream fos = new FileOutputStream("serial_personFile2.bin");
        ObjectOutputStream oos = new ObjectOutputStream(fos);

        Client client = new Client(2, "fullName", 21);
        oos.writeObject(client);
        oos.close();

        System.out.println("Start deserialization");
        FileInputStream fis = new FileInputStream("serial_personFile2.bin");
        ObjectInputStream ios = new ObjectInputStream(fis);

        System.out.println(ios.readObject());
        ios.close();
    }

}

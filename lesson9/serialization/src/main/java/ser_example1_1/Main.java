package ser_example1_1;

import java.io.*;

/**
 * Person расширяет Serializable. Для сериализации дочерних классов не требуется никаких дополнительных действий
 */
public class Main {

    public static void main(String[] args) {
        try {
            doMethod1();
            doMethod2();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    //Простая сериализация в файл
    private static void doMethod1() throws IOException, ClassNotFoundException {
        String path = "serial_personFile1_1.bin";

        try (FileOutputStream fos = new FileOutputStream(path);
             ObjectOutputStream oos = new ObjectOutputStream(fos)) {

            Client client = new Client(1, "fullName", 20, "my_login");
            oos.writeObject(client);
        }

        System.out.println("Start deserialization");
        try(FileInputStream fis = new FileInputStream(path);
            ObjectInputStream ios = new ObjectInputStream(fis)) {
            System.out.println(ios.readObject());
        }
    }

    //Запись нескольких объектов в файл
    private static void doMethod2() throws IOException, ClassNotFoundException {
        String path = "serial_personFile1_2.bin";

        try(FileOutputStream fos = new FileOutputStream(path);
            ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            Client client = new Client(1, "fullName", 20, "my_login");
            //1й объект
            oos.writeObject(client);
            //2й объект
            oos.writeObject(client.getLogin());
        }

        System.out.println("Start deserialization");
        try(FileInputStream fis = new FileInputStream(path);
            ObjectInputStream ios = new ObjectInputStream(fis)) {

            //Порядок имеет значение
            Client client = (Client) ios.readObject();

            Object sss = ios.readObject();

            System.out.println(client);
            System.out.println(sss);
        }
    }

}

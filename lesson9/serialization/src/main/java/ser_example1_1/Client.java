package ser_example1_1;

public class Client extends Person {

    private long id;
    private transient String login;
//    private Car bmw = new Car("BMW");

    public Client(long id, String name, int age, String login) {
        super(name, age);

        this.id = id;
        this.login = login;
        System.out.println("call Client(long id, String name, int age, String login) constructor");
    }

    public long getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @Override
    public String toString() {
        return "Client{" +
                "id=" + id +
                ", name='" + getName() + '\'' +
                ", age=" + getAge() +
                ", login=" + getLogin() +
                '}';
    }
}

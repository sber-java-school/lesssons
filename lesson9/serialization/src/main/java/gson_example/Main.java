package gson_example;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Демо JSON-сериализации
 */
public class Main {

    public static void main(String[] args) {
        Client client = new Client(1, "fullName", 20);

        Gson gson = new GsonBuilder().create();
        String jsonClient = gson.toJson(client);

        System.out.println("");
        System.out.println("JSON = " + jsonClient);
        System.out.println("");

        System.out.println("Start deserialization");
        Client deserClient = gson.fromJson(jsonClient, Client.class);
        System.out.println(deserClient);

    }
}

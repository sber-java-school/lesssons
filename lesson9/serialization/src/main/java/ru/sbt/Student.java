package ru.sbt;

import java.io.Serializable;

/**
 * Смотрим на байтовое представление объекта
 */
public class Student implements Serializable {
    private String name;

    public Student(String name) {
        this.name = name;
    }
}


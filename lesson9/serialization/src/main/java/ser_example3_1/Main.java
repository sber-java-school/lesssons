package ser_example3_1;

import java.io.*;

/**
 * Для работы Externalizable обязательно наличие публичного дефолтного конструктора.
 * Каждый дочерний класс, должен переопределять методы writeExternal(ObjectOutput out) и readExternal(ObjectInput in)
 */
public class Main {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        FileOutputStream fos = new FileOutputStream("ext_personFile1");
        ObjectOutputStream oos = new ObjectOutputStream(fos);

        Client client = new Client(2, "fullName", 21);
        oos.writeObject(client);

        System.out.println("Start deserialization");
        FileInputStream fis = new FileInputStream("ext_personFile1");
        ObjectInputStream ios = new ObjectInputStream(fis);
        System.out.println(ios.readObject());
    }
}

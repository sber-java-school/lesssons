package jaxb_example;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Демо XML-сериализации
 */
public class Main {

    public static void main(String[] args) throws JAXBException, IOException {
        Client client = new Client(1, "fullName", 20);

        JAXBContext jaxbContext = JAXBContext.newInstance( Client.class );
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

        System.out.println("Start serialization");
        jaxbMarshaller.marshal(client, System.out);
        System.out.println();

        String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><client><age>20</age><name>fullName</name><id>1</id></client>";
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

        InputStream is = new ByteArrayInputStream(xml.getBytes());
        System.out.println("\r\nStart deserialization");
        Client unmarshallClient = (Client) unmarshaller.unmarshal(is);
        System.out.println(unmarshallClient);
    }

}

package jaxb_example;


public class Person {

    private String name;
    private int age;

    public Person() {
        System.out.println("call Person() constructor");
    }

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
        System.out.println("call Person(String name, int age) constructor");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}

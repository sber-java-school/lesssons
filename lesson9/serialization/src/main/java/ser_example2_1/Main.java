package ser_example2_1;

import java.io.*;

/**
 * Используем дополнительную возможность записи информации в файл.
 */
public class Main {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        FileOutputStream fos = new FileOutputStream("ser_personFile3.bin");
        ObjectOutputStream oos = new ObjectOutputStream(fos);

        Client client = new Client(1555999, "fullName", 21);
        oos.writeObject(client);
        oos.close();

        System.out.println("Start deserialization");
        FileInputStream fis = new FileInputStream("ser_personFile3.bin");
        ObjectInputStream ios = new ObjectInputStream(fis);
        System.out.println(ios.readObject());
    }

}

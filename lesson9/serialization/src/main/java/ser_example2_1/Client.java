package ser_example2_1;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Client extends Person implements Serializable {

    private long id;

    public Client(long id, String name, int age) {
        super(name, age);
        this.id = id;
        System.out.println("call Client(long id, String name, int age) constructor");
    }

    public long getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Client{" +
                "id=" + id +
                ", name='" + getName() + '\'' +
                ", age=" + getAge() +
                '}';
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        out.defaultWriteObject();

        out.writeInt(getAge());
        out.writeUTF("This is Serialization Lesson!");
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();

        int age = in.readInt();
        System.out.println("Age = " + age);
        setAge(age);

        String message = in.readUTF();
        System.out.println(message);
    }
}

package ser_example4_memCache;

import java.io.Serializable;

public class GraphNode  implements Serializable {
    private GraphNode nextNode;

    public GraphNode getNextNode() {
        return nextNode;
    }

    public void setNextNode(GraphNode nextNode) {
        this.nextNode = nextNode;
    }
}

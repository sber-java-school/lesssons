package ser_example4_memCache;

import java.io.*;

/**
 * Демонстрация кэширования ссылок внутри потока
 */
public class Main {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        demo1();
        demo2();
    }

    private static void demo1() throws IOException, ClassNotFoundException {
        GraphNode cyclicGraph = new GraphNode();
        cyclicGraph.setNextNode(cyclicGraph);

        FileOutputStream fos = new FileOutputStream("ser_example4_memCache");
        ObjectOutputStream oos = new ObjectOutputStream(fos);

        oos.writeObject(cyclicGraph);

        System.out.println("Start deserialization");
        FileInputStream fis = new FileInputStream("ser_example4_memCache");
        ObjectInputStream ios = new ObjectInputStream(fis);

        GraphNode graphNode = (GraphNode) ios.readObject();
        System.out.println(graphNode == graphNode.getNextNode());
    }

    private static void demo2() throws IOException, ClassNotFoundException {
        GraphNode cyclicGraph = new GraphNode();
        cyclicGraph.setNextNode(cyclicGraph);

        FileOutputStream fos = new FileOutputStream("ser_example4_memCache");
        ObjectOutputStream oos = new ObjectOutputStream(fos);

        oos.writeObject(cyclicGraph);
        oos.close();

        fos = new FileOutputStream("ser_example4_memCache");
        oos = new ObjectOutputStream(fos);
        //Если не закрыть поток, то из-за кэширования, новое поле не запишется, т.к. объект уже есть в кэше!
        cyclicGraph.setNextNode(new GraphNode());

        oos.writeObject(cyclicGraph);
        oos.close();

        System.out.println("Start deserialization");
        FileInputStream fis = new FileInputStream("ser_example4_memCache");
        ObjectInputStream ios = new ObjectInputStream(fis);

        GraphNode graphNode = (GraphNode) ios.readObject();
        System.out.println(graphNode == graphNode.getNextNode());
    }
}

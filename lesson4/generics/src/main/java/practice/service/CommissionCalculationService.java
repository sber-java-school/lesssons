package practice.service;

import practice.model.AbstractAccount;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

public class CommissionCalculationService <T extends AbstractAccount> {

    //Прошу не обращать внимание на данный код, просто поверить,что тут происходит расчет коммисиии
    public List<BigDecimal> getCalculatedCommissionList(List<T> accounts){
        return accounts
                .stream()
                .map(AbstractAccount::calculateCommission)
                .collect(Collectors.toList());
    }
}

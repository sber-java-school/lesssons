package practice;

import practice.model.Account;
import practice.model.CreditAccount;
import practice.service.CommissionCalculationService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        CommissionCalculationService<CreditAccount> creditCommissionService = new CommissionCalculationService<>();
        CommissionCalculationService<Account> commissionService = new CommissionCalculationService<>();

        Account firstAccount = new Account();
        firstAccount.setId(1);
        firstAccount.setBalance(BigDecimal.valueOf(10000L));
        firstAccount.setNumber(427612141);

        CreditAccount secondAccount = new CreditAccount();
        secondAccount.setId(2);
        secondAccount.setBalance(BigDecimal.valueOf(10000L));
        secondAccount.setNumber(126413031);

        CreditAccount thirdAccount = new CreditAccount();
        thirdAccount.setId(3);
        thirdAccount.setBalance(BigDecimal.valueOf(10000L));
        thirdAccount.setNumber(1221335163);

        CreditAccount fourthAccount = new CreditAccount();
        fourthAccount.setId(4);
        fourthAccount.setBalance(BigDecimal.valueOf(10000L));
        fourthAccount.setNumber(516598162);

        Account fifthAccount = new Account();
        fifthAccount.setId(5);
        fifthAccount.setBalance(BigDecimal.valueOf(10000L));
        fifthAccount.setNumber(5465150);

        List<CreditAccount> creditAccounts = new ArrayList<>();
        creditAccounts.add(secondAccount);
        creditAccounts.add(thirdAccount);
        creditAccounts.add(fourthAccount);

        List<Account> accounts = new ArrayList<>();
        accounts.add(firstAccount);
        accounts.add(secondAccount);
        accounts.add(thirdAccount);
        accounts.add(fourthAccount);
        accounts.add(fifthAccount);

        System.out.println(creditCommissionService.getCalculatedCommissionList(creditAccounts));
        System.out.println(commissionService.getCalculatedCommissionList(accounts));
    }
}

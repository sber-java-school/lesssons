package practice.model;

import java.math.BigDecimal;

//Абстрактный класс счета
public abstract class AbstractAccount {
    private int id;
    private int number;

    //метод расчета комиссии, например, при переводе
    public abstract BigDecimal calculateCommission();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}

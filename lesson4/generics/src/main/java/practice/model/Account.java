package practice.model;

import java.math.BigDecimal;

//Класс, расширяющий абстакный класс счета
public class Account extends AbstractAccount {

    private BigDecimal balance;
    private Boolean onCommissionToggle = false;
    private BigDecimal commissionBoundary = BigDecimal.valueOf(50000L);

    public BigDecimal getBalance() {
        return balance;
    }

    public Boolean getOnCommissionToggle() {
        return onCommissionToggle;
    }

    public BigDecimal getCommissionBoundary() {
        return commissionBoundary;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public void setOnCommissionToggle(Boolean onCommissionToggle) {
        this.onCommissionToggle = onCommissionToggle;
    }

    public void setCommissionBoundary(BigDecimal commissionBoundary) {
        this.commissionBoundary = commissionBoundary;
    }

    //переопределение метода расчета комисии:
    //если onCommissionToggle==true, позвращаем фиксированную суммму комиссии
    //если onCommissionToggle==false, отнимаем от commissionBoundary фиксированную величину, для уменшения порога бесплатных переводов
    //в случае, если commissionBoundary < 0, переводим onCommissionToggle=true
    @Override
    public BigDecimal calculateCommission() {
        if (onCommissionToggle) {
            return BigDecimal.valueOf(10L);
        } else {
            commissionBoundary.subtract(BigDecimal.valueOf(100L));
            if (commissionBoundary.compareTo(BigDecimal.valueOf(0L)) != 0) ;
            onCommissionToggle = true;
            return BigDecimal.valueOf(0L);
        }
    }
}

package practice.model;

import java.math.BigDecimal;

//Класс кредитного счета
public class CreditAccount extends Account {
    private BigDecimal creditAmount;
    private BigDecimal addingPercentageCommission = BigDecimal.valueOf(50L);

    //переопределение метода расчета комисии:
    //если onCommissionToggle==true, позвращаем фиксированную суммму комиссии плюс дополнительную комиссию для переводов кредитных счетов
    //если onCommissionToggle==false, отнимаем от commissionBoundary фиксированную величину, для уменшения порога бесплатных переводов
    //в случае, если commissionBoundary < 0, переводим onCommissionToggle=true
    @Override
    public BigDecimal calculateCommission() {
        if (getOnCommissionToggle()) {
            return BigDecimal.valueOf(10L).add(addingPercentageCommission);
        } else {
            setCommissionBoundary(getCommissionBoundary().subtract(BigDecimal.valueOf(100L)));
            if (getCommissionBoundary().compareTo(BigDecimal.valueOf(0L)) != 0) ;
            setOnCommissionToggle(true);
            return BigDecimal.valueOf(0L);
        }
    }
}

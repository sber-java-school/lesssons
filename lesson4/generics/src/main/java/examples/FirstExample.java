package examples;

import java.util.ArrayList;
import java.util.List;

public class FirstExample {
    public static void main(String[] args) {
        List list = new ArrayList();
        list.add("Hello");
        String text = list.get(0) + ", world!";
        System.out.print(text);

        List secondList = new ArrayList();
        secondList.add("Hello!");
        secondList.add(123);
        for (Object str : secondList) {
            System.out.println((String) str);
        }
    }
}

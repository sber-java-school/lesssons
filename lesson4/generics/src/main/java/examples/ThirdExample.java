package examples;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ThirdExample {
    public static void main(String[] args) {
        String[] strings = new String[] {"a", "b", "c"};
        Object[] arr = strings;
        System.out.println(Arrays.asList(arr));
        arr[0] = 42;

//        List<Integer> ints = Arrays.asList(1,2,3);
//        List<Number> nums = ints; // compile-time error. Проблема обнаружилась на этапе компиляции
//        nums.set(2, 3.14);
//        assert ints.toString().equals("[1, 2, 3.14]");
////
        List<Integer> coVarInts = new ArrayList<>();
        List<? extends Number> coVarNums = coVarInts;

        List<Number> contrVarNums = new ArrayList<>();
        List<? super Integer> contrVarInts = contrVarNums;
    }
}

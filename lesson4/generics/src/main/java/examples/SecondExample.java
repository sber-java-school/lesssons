package examples;

import examples.liskovexample.controller.OrdersStockController;
import examples.liskovexample.model.Order;
import examples.liskovexample.service.OrderStockAndPackValidator;

import java.util.Arrays;
import java.util.List;

public class SecondExample {
    public static void main(String[] args) {
        OrdersStockController controller = new OrdersStockController(new OrderStockAndPackValidator());

        Order order1 = new Order();
        order1.setId(1);
        order1.setInStock(true);
        order1.setPrivate(true);

        Order order2 = new Order();
        order2.setId(1);
        order2.setInStock(true);
        order2.setPrivate(true);

        List<Order> orders = Arrays.asList(order1, order1);

        System.out.println(controller.isOrdersValid(orders));
    }
}

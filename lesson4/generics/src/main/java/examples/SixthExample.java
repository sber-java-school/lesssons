package examples;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class SixthExample {
    //PECS
    public static void main(String[] args) {
        List<Integer> ints = Arrays.asList(1,2);
//        System.out.println(reverse(ints));

    }

    public static List<?> reverse(List<?> list) {
        List<Object> tmp = new ArrayList<>(list);
        for (int i = 0; i < list.size(); i++) {
            tmp.set(i, list.get(list.size() - i - 1));
        }
        return tmp;
    }

//
//    public static void reverse(List<?> list) {
//        genericReverse(list);
//    }
//
//    public static <T> void genericReverse(List<T> list) {
//        List<T> tmp = new ArrayList<>(list);
//        for (int i = 0; i < list.size(); i++) {
//            list.set(i, tmp.get(list.size()-i-1));
//        }
//    }
}

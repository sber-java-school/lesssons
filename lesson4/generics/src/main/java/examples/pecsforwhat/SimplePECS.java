package examples.pecsforwhat;

import examples.liskovexample.model.pecsexample.ChildClass;
import examples.liskovexample.model.pecsexample.MoreChildClass;
import examples.liskovexample.model.pecsexample.ParentClass;

import java.util.ArrayList;
import java.util.List;

public class SimplePECS {
    public static void main(String[] args) {
        ArrayList<Object> list = new ArrayList<>();
        list.add(new MoreChildClass());
        list.add(new ChildClass());
//        someMethodConsumer(list);
    }

//    public static void someMethodProducer(List<? extends ChildClass> list) {
//        list.add(new ParentClass());
//        list.add(new ChildClass());
//        list.add(new MoreChildClass());
//        list.add(null);
//
//        ParentClass parentClass = list.get(0);
//        ChildClass childClass = list.get(0);
//        MoreChildClass moreChildClass =  list.get(0);
//    }
//
//    public static void someMethodConsumer(List<? super ChildClass> list) {
//        list.add(new MoreChildClass());
//        list.add(new ChildClass());
//        list.add(new ParentClass());
//
//        ParentClass parentClass = list.get(0);
//        ChildClass childClass = list.get(0);
//        MoreChildClass moreChildClass = list.get(0);
//
//    }
}

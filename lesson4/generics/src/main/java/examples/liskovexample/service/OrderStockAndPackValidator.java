package examples.liskovexample.service;


import examples.liskovexample.model.Order;

import java.util.List;

public class OrderStockAndPackValidator extends OrderStockValidator {

    @Override
    public boolean isValid(List<Order> orders) {
        for (Order item : orders) {
            if (!item.isInStock() || !item.isPacked()) {
                return false;
            }
        }
        return true;
    }
}
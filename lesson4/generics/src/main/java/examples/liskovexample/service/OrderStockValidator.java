package examples.liskovexample.service;

import examples.liskovexample.model.Order;

import java.util.List;

public class OrderStockValidator {

    public boolean isValid(List<Order> orders) {
        for (Order order : orders) {
            if (!order.isInStock()) {
                return false;
            }
        }
        return true;
    }
}

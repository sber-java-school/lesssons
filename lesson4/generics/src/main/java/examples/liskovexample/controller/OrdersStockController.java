package examples.liskovexample.controller;

import examples.liskovexample.model.Order;
import examples.liskovexample.service.OrderStockValidator;

import java.util.List;

public class OrdersStockController {

    private final OrderStockValidator validator;

    public OrdersStockController(OrderStockValidator validator) {
        this.validator = validator;
    }

    public boolean isOrdersValid (List<Order> orders) {
        boolean valid = validator.isValid(orders);
        if (!valid) {
            throw new IllegalStateException();
        }
        return validator.isValid(orders);
    }
}

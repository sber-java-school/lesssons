package examples;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class FourthExample {
    //PECS
    public static void main(String[] args) {
        List<Number> nums = Arrays.asList(4.1F, 0.2F);
        List<Integer> ints = Arrays.asList(1,2);
        Collections.copy(nums, ints);
        System.out.println(ints);
        System.out.println(nums);
    }
}

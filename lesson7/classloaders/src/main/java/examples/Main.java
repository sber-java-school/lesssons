package examples;

public class Main {

    public static void main(String[] args) throws ClassNotFoundException {
        System.out.println(1);

        Person person = new Person();
        System.out.println(2);
        Class<? extends Person> aClass1 = person.getClass();

        Class<?> aClass2 = Main.class.getClassLoader().loadClass("examples.Person");
        System.out.println(3);

        System.out.println(aClass1 == aClass2);
    }

}

package examples;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLClassLoader;

public class Main2 {

    public static void main(String[] args) throws ClassNotFoundException, MalformedURLException {
        System.out.println(1);

        Person person = new Person();
        System.out.println(2);
        Class<? extends Person> aClass1 = person.getClass();

        URL[] urls = new URL[]{URI.create("file:///C:/Users/atsar/IdeaProjects/lesssons/lesson7/classloaders/target/classes/examples/Person.class").toURL()};
        URLClassLoader urlClassLoader = new URLClassLoader(urls, null);
        Class<?> aClass2 = urlClassLoader.loadClass("examples.Person");
        System.out.println(3);

        System.out.println(aClass1 == aClass2);
    }
}

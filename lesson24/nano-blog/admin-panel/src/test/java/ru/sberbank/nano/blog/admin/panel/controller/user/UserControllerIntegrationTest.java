package ru.sberbank.nano.blog.admin.panel.controller.user;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.sberbank.nano.blog.admin.panel.AdminPanelApplication;
import ru.sberbank.nano.blog.admin.panel.dto.SaveUserDto;
import ru.sberbank.nano.blog.admin.panel.service.UserService;
import ru.sberbank.nano.blog.persistence.layer.entity.User;

import static org.hamcrest.core.Is.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = AdminPanelApplication.class)
@AutoConfigureMockMvc
public class UserControllerIntegrationTest {

    @Autowired
    private MockMvc mvc;
    @Autowired
    private UserService userService;

    @Test
    public void givenUsers_whenGetUsers_thenStatus200() throws Exception {
        SaveUserDto userDto = createUserDto();
        User user = userService.registration(userDto);

        String basicCredentials = Base64.encode("test_login:test_password".getBytes());
        mvc.perform(get("/api/users").header("Authorization", "Basic " + basicCredentials).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].firstName", is("fName")))
                .andExpect(jsonPath("$[0].lastName", is("lName")));
    }

    private SaveUserDto createUserDto() {
        SaveUserDto dto = new SaveUserDto();
        dto.setLogin("test_login");
        dto.setPassword("test_password");
        dto.setFirstName("fName");
        dto.setLastName("lName");
        return dto;
    }
}

package ru.sberbank.nano.blog.admin.panel.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sberbank.nano.blog.admin.panel.dto.SaveBlogPostDto;
import ru.sberbank.nano.blog.persistence.layer.entity.BlogPost;
import ru.sberbank.nano.blog.persistence.layer.entity.User;
import ru.sberbank.nano.blog.persistence.layer.repository.BlogPostRepository;
import ru.sberbank.nano.blog.persistence.layer.repository.UserRepository;

import java.util.List;

@Service
public class BlogPostService {

    private final BlogPostRepository blogPostRepository;
    private final UserRepository userRepository;

    @Autowired
    public BlogPostService(BlogPostRepository blogPostRepository,
                           UserRepository userRepository) {
        this.blogPostRepository = blogPostRepository;
        this.userRepository = userRepository;
    }

    public List<BlogPost> getAll() {
        return blogPostRepository.findAll();
    }

    public BlogPost getById(Long blogPostId) {
        return blogPostRepository.findById(blogPostId).orElseThrow(RuntimeException::new);
    }

    public BlogPost saveBlogPost(SaveBlogPostDto saveBlogPostDto) {
        User author = userRepository.findById(saveBlogPostDto.getUserId())
                .orElseThrow(RuntimeException::new);
        BlogPost blogPost = new BlogPost();
        blogPost.setTitle(saveBlogPostDto.getTitle());
        blogPost.setBody(saveBlogPostDto.getBody());
        blogPost.setAuthor(author);
        return blogPostRepository.save(blogPost);
    }

}

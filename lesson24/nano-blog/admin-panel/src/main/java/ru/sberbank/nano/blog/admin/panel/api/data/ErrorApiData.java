package ru.sberbank.nano.blog.admin.panel.api.data;

public class ErrorApiData {
    private final String errorDescription;

    public ErrorApiData(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public String getErrorDescription() {
        return errorDescription;
    }
}

package ru.sberbank.nano.blog.admin.panel.api.data.blog;

import ru.sberbank.nano.blog.admin.panel.api.data.user.UserApiData;

public class BlogPostApiData {
    private Long id;
    private String title;
    private String body;
    private UserApiData author;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public UserApiData getAuthor() {
        return author;
    }

    public void setAuthor(UserApiData author) {
        this.author = author;
    }
}

package ru.sberbank.nano.blog.admin.panel.controller.auth;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.sberbank.nano.blog.admin.panel.api.converter.UserApiConverter;
import ru.sberbank.nano.blog.admin.panel.api.data.auth.RegistrationApiRequest;
import ru.sberbank.nano.blog.admin.panel.api.data.user.UserApiData;
import ru.sberbank.nano.blog.admin.panel.dto.SaveUserDto;
import ru.sberbank.nano.blog.admin.panel.service.UserService;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/auth")
public class AuthController {

    private final UserService userService;
    private final UserApiConverter userApiConverter;

    public AuthController(UserService userService, UserApiConverter userApiConverter) {
        this.userService = userService;
        this.userApiConverter = userApiConverter;
    }

    @PostMapping("/registration")
    public UserApiData login(@Valid @RequestBody RegistrationApiRequest request) {
        SaveUserDto saveUserDto = getSaveUserDto(request);
        return userApiConverter.convert(userService.registration(saveUserDto));
    }

    private SaveUserDto getSaveUserDto(RegistrationApiRequest request) {
        SaveUserDto saveUserDto = new SaveUserDto();
        saveUserDto.setLogin(request.getLogin());
        saveUserDto.setPassword(request.getPassword());
        saveUserDto.setFirstName(request.getFirstName());
        saveUserDto.setLastName(request.getLastName());
        return saveUserDto;
    }
}

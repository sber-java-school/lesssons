package ru.sberbank.nano.blog.admin.panel.api.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.sberbank.nano.blog.admin.panel.api.data.blog.BlogPostApiData;
import ru.sberbank.nano.blog.persistence.layer.entity.BlogPost;

@Component
public class BlogPostApiConverter {
    private final UserApiConverter userApiConverter;

    @Autowired
    public BlogPostApiConverter(UserApiConverter userApiConverter) {
        this.userApiConverter = userApiConverter;
    }

    public BlogPostApiData convert(BlogPost blogPost) {
        BlogPostApiData apiData = new BlogPostApiData();
        apiData.setId(blogPost.getId());
        apiData.setTitle(blogPost.getTitle());
        apiData.setBody(blogPost.getBody());
        apiData.setAuthor(userApiConverter.convert(blogPost.getAuthor()));
        return apiData;
    }
}

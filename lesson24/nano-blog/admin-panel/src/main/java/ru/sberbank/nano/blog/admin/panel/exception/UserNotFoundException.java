package ru.sberbank.nano.blog.admin.panel.exception;

import static java.lang.String.format;
import static org.springframework.http.HttpStatus.NOT_FOUND;

//@ResponseStatus(code = NOT_FOUND, reason = "User not found")
public class UserNotFoundException extends BaseException {
    public UserNotFoundException(Long userId) {
        super(NOT_FOUND, format("User with id %d not found", userId));
    }
}

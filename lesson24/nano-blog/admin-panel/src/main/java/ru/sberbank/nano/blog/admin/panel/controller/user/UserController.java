package ru.sberbank.nano.blog.admin.panel.controller.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.sberbank.nano.blog.admin.panel.api.converter.UserApiConverter;
import ru.sberbank.nano.blog.admin.panel.api.data.ErrorApiData;
import ru.sberbank.nano.blog.admin.panel.api.data.user.UserApiData;
import ru.sberbank.nano.blog.admin.panel.exception.UserNotFoundException;
import ru.sberbank.nano.blog.admin.panel.service.UserService;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private final UserService userService;
    private final UserApiConverter userApiConverter;

    @Autowired
    public UserController(UserService userService,
                          UserApiConverter userApiConverter) {
        this.userService = userService;
        this.userApiConverter = userApiConverter;
    }

    @GetMapping
    public List<UserApiData> getAll() {
        return userService.getAll()
                .stream()
                .map(userApiConverter::convert)
                .collect(Collectors.toList());
    }

    @GetMapping("/{user_id}")
    public UserApiData getUserById(@NotNull @PathVariable("user_id") Long userId) {
        return userApiConverter.convert(userService.getUserById(userId));
    }

    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<ErrorApiData> handleException(UserNotFoundException exception) {
        ErrorApiData errorApiData = new ErrorApiData(exception.getMessage());
        return ResponseEntity
                .status(exception.getHttpStatus())
                .body(errorApiData);
    }

}

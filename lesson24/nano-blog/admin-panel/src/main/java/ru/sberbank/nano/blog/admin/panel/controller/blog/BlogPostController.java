package ru.sberbank.nano.blog.admin.panel.controller.blog;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import ru.sberbank.nano.blog.admin.panel.api.converter.BlogPostApiConverter;
import ru.sberbank.nano.blog.admin.panel.api.data.blog.BlogPostApiData;
import ru.sberbank.nano.blog.admin.panel.api.data.blog.CreateBlogPostApiRequest;
import ru.sberbank.nano.blog.admin.panel.dto.SaveBlogPostDto;
import ru.sberbank.nano.blog.admin.panel.service.BlogPostService;
import ru.sberbank.nano.blog.persistence.layer.entity.BlogPost;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/blog/posts")
public class BlogPostController {

    private final BlogPostService blogPostService;
    private final BlogPostApiConverter blogPostApiConverter;

    @Autowired
    public BlogPostController(BlogPostService blogPostService,
                              BlogPostApiConverter blogPostApiConverter) {
        this.blogPostService = blogPostService;
        this.blogPostApiConverter = blogPostApiConverter;
    }

    @GetMapping
    public List<BlogPostApiData> getAll() {
        return blogPostService
                .getAll()
                .stream()
                .map(blogPostApiConverter::convert)
                .collect(Collectors.toList());
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public BlogPostApiData createPost(@Valid @RequestBody CreateBlogPostApiRequest request) {
        SaveBlogPostDto blogPostDto = new SaveBlogPostDto();
        blogPostDto.setTitle(request.getTitle());
        blogPostDto.setBody(request.getBody());
        blogPostDto.setUserId(request.getAuthorId());
        BlogPost blogPost = blogPostService.saveBlogPost(blogPostDto);
        return blogPostApiConverter.convert(blogPost);
    }
}

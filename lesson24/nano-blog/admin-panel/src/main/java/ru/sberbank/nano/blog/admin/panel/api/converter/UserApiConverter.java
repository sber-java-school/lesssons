package ru.sberbank.nano.blog.admin.panel.api.converter;

import org.springframework.stereotype.Component;
import ru.sberbank.nano.blog.admin.panel.api.data.user.UserApiData;
import ru.sberbank.nano.blog.persistence.layer.entity.User;

@Component
public class UserApiConverter {

    public UserApiData convert(User user) {
        UserApiData userApiData = new UserApiData();
        userApiData.setId(user.getId());
        userApiData.setFirstName(user.getFirstName());
        userApiData.setLastName(user.getLastName());
        return userApiData;
    }
}

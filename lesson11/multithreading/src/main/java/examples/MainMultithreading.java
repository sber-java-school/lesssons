package examples;

import java.util.concurrent.TimeUnit;

public class MainMultithreading {

    public static final MainMultithreading.Foo FOO = new Foo();

    public static final Object o1 = new Object();
    public static final Object o2 = new Object();
    public static final MainMultithreading.Counter COUNTER = new Counter(0);

    public static void main(String[] args) throws ClassNotFoundException, InterruptedException {

        new Thread(() -> {
            synchronized (o1) {
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                synchronized (o2) {
                    System.out.printf("Finish from %s/n", Thread.currentThread().getName());
                }
            }
        }).start();

        new Thread(() -> {
            synchronized (o2) {
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                synchronized (o1) {
                    System.out.printf("Finish from %s/n", Thread.currentThread().getName());
                }
            }
        }).start();
    }


    static class Counter {
        private Object lock = new Object();

        private volatile int i;

        public Counter(int i) {
            this.i = i;
        }

        public int incAndGet() {
//            synchronized (lock) {
                int localI = i;
                localI = localI + 1;
                i = localI;
                return localI;
//            }
        }

        public int decAndGet() {
            synchronized (lock) {
                return i = i - 1;
            }
        }
    }

    static class Foo {

        private final Object someObject = new Object();

        public void someMethod() throws InterruptedException {

            synchronized (someObject) {
                // only one thread in a moment here
                someObject.wait();
            }
        }

        public void wakeUp() {

            synchronized (someObject) {
                // only one thread in a moment here
                someObject.notify();
            }
        }
    }

}

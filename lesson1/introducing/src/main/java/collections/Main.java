package collections;

import java.util.ArrayList;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        int[] i = new int[2];
        System.out.println(i.length);
        i[0] = 1;
        System.out.println(Arrays.toString(i));
        i[1] = 2;
        System.out.println(Arrays.toString(i));

        int[] a = new int[]{};
        System.out.println(a.length);
        a[1] = 1;
        System.out.println(Arrays.toString(a));

        ArrayList<Double> arrayList = new ArrayList<>();

        int count = 0;

        while (count < 100){
            arrayList.add(Math.random());
            count++;
        }
        System.out.println(arrayList);
        System.out.println(arrayList.size());
    }
}

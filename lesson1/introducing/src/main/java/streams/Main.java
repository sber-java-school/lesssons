package streams;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        ArrayList<Double> list = new ArrayList<>();

        int count = 0;

        while (count < 100){
            list.add(Math.random() * 10);
            count++;
        }

        System.out.println("list:");
        System.out.println(list);

        List<Double> newList = list.stream()
                .filter(x -> x > 5)
                .collect(Collectors.toList());

        System.out.println("new-list:");
        System.out.println(newList);
        System.out.println(newList.size());
    }
}

package exceptions;

import exceptions.customexceptions.FirstSimpleException;
import exceptions.customexceptions.SecondSimpleException;

public class ExampleWithMultiplyCatchException {
    public static void main(String[] args) {
        try {
            getSomethingWithSecondSimpleException();
        } catch (FirstSimpleException | SecondSimpleException e){
            System.out.println(e.getClass().getSimpleName());
        }
    }

    private static Integer someGetter() {
        return null;
    }

    private static void getSomethingWithFirstSimpleException() {
        throw new FirstSimpleException();
    }

    private static void getSomethingWithSecondSimpleException() {
        try {
            int i = someGetter();
        } catch (NullPointerException e) {
            System.out.println("Exception!!");
            System.out.println(e.getClass().getSimpleName());
            throw new SecondSimpleException();
        }
    }
}

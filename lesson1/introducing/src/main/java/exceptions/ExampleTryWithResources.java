package exceptions;

import java.io.FileOutputStream;
import java.io.IOException;

public class ExampleTryWithResources {

    public static void main(String[] args) throws IOException {
        tryCatchFinally();
        tryWithResources();
    }

    public static void tryCatchFinally() throws IOException {
        FileOutputStream output = new FileOutputStream("introducing/src/main/resources/info.txt");
        try {
            output.write(23);
            output.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            output.close();
        }
    }

    public static void tryWithResources() {
        try (FileOutputStream output = new FileOutputStream("introducing/src/main/resources/info.txt")) {
            output.write(1);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

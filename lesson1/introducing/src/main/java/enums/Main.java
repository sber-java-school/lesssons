package enums;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        Seasons spring = Seasons.SPRING;
        System.out.println(spring);
        System.out.println(spring.ordinal());
        System.out.println(spring.name());
        System.out.println(Arrays.toString(Seasons.values()));
    }

    public enum Seasons {
        WINTER(1,"Cold"),
        SPRING(2,"Warm"),
        SUMMER(3,"Hot"),
        AUTUMN(4,"Сhilly");

        private final int id;
        private final String description;

        Seasons(int id, String description) {
            this.id = id;
            this.description = description;
        }

        public int getId() {
            return id;
        }

        public String getDescription() {
            return description;
        }

        @Override
        public String toString() {
            return "Seasons{" +
                    "description='" + description + '\'' +
                    '}';
        }
    }
}

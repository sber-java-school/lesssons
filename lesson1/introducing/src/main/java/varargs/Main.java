package varargs;

public class Main {

    public static void main(String[] args) {
        System.out.println(sum(1,2,3,45,25));
    }

    public static Integer sum(Integer... integers) {
        Integer result = 0;
        for(int i = 0; i < integers.length; i++) {
            result += integers[i];
        }
        return result;
    }
}

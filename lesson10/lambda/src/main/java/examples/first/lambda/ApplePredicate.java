package examples.first.lambda;

import examples.Apple;

public interface ApplePredicate {

    boolean test(Apple apple);
}

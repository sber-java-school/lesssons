package examples.first.lambda;

import examples.Apple;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class Main {

    ApplePredicate greenPred = (apple) -> "green".equals(apple.getColor());


    public static void main(String[] args) {
        Apple green10 = new Apple("green", 10d);
        Apple green1000 = new Apple("green", 1000d);
        Apple red10 = new Apple("red", 10d);
        Apple red1000 = new Apple("red", 1000d);
        List<Apple> inventory = Arrays.asList(green10, green1000, red10, red1000);

        List<Apple> heavy = filterApples(inventory, new AppleHeavyWeightPredicate());
        List<Apple> green = filterApples(inventory, new AppleGreenColorPredicate());

        System.out.println(heavy);
        System.out.println(green);


        System.out.println("Анонимные классы");
        List<Apple> heavy2 = filterApples(inventory, new ApplePredicate() {
            @Override
            public boolean test(Apple apple) {
                return apple.getWeight() > 150;
            }
        });
        List<Apple> green2 = filterApples(inventory, new ApplePredicate() {
            @Override
            public boolean test(Apple apple) {
                return "green".equals(apple.getColor());
            }
        });

        System.out.println(heavy);
        System.out.println(green);


        System.out.println("Lambda");
        List<Apple> heavy3 = filterApples(inventory, apple -> apple.getWeight() > 150);
        List<Apple> green3 = filterApples(inventory, apple -> {
            System.out.println("");
            return "green".equals(apple.getColor());
        });

        System.out.println(heavy3);
        System.out.println(green3);

        heavy.sort(Comparator.comparingDouble(Apple::getWeight));
    }

    public static List<Apple> filterApples(List<Apple> inventory, ApplePredicate p) {
        List<Apple> result = new ArrayList<>();
        for (Apple a : inventory) {
            if (p.test(a)) {
                result.add(a);
            }
        }
        return result;
    }

    public void checkPredicate(List<Apple> apples) {
        List<Apple> green4 = filterApples(apples, greenPred);
    }
}

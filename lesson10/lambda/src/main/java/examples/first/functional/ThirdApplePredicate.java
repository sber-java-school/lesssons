package examples.first.functional;

import examples.Apple;

@FunctionalInterface
public interface ThirdApplePredicate {

    boolean test(Apple apple);
}

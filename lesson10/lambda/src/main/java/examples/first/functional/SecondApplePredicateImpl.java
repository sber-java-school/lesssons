package examples.first.functional;

import examples.Apple;

//@FunctionalInterface
public interface SecondApplePredicateImpl extends SecondApplePredicate {

    boolean test(Apple apple);

    default boolean test2(Apple apple) {
        throw new RuntimeException();
    }

    default boolean test3(Apple apple) {
        throw new RuntimeException();
    }

    default boolean test4(Apple apple) {
        throw new RuntimeException();
    }

    default boolean test5(Apple apple) {
        throw new RuntimeException();
    }
}

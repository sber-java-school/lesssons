package examples.first.functional;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Locale;
import java.util.function.Predicate;
import java.util.stream.IntStream;

public class Main {


    public static void main(String[] args) {
        String inStr = "Лямбда-выражения повышают эффективность Java";
        String outStr;
        System.out.println("Этo исходная строка: " + inStr);
        // Ниже приведено простое лямбда-выражение, преобразующее в прописные все буквы в исходной строке,
        // передаваемой методу stringOp()
        outStr = stringOp(str -> str.toUpperCase(Locale.ROOT), inStr);
        System.out.println("Этo строка прописными буквами: " + outStr);
        // А здесь передается блочное лямбда-выражение, удаляющее пробелы из исходной символьной строки
        outStr = stringOp((str) -> {
            String result = "";
            int i;
            for (i = 0; i < str.length(); i++)
                if (str.charAt(i) != ' ') {
                    result += str.charAt(i);
                }
            return result;
        }, inStr);
        System.out.println("Этo строка с удаленными пробелами: " + outStr);

        // Можно, конечно, передать и экземпляр функционального интерфейса SomeFunc ,созданный в предыдущем лямбда-выражении.
        // Например, после следующего объявления ссылка reverse делается на экземпляр интерфейса StringFunc
        SomeFunc reverse = (str) -> {
            String result = "";
            int i;
            for (i = str.length() - 1; i >= 0; i--)
                result += str.charAt(i);
            return result;
        };
        // А теперь ссылку reverse можно передать в качестве первого параметра методУ stringOp(),
        // поскольку она делается на объект типа SomeFunc
        System.out.println("Этo обращенная строка: " + stringOp(reverse, inStr));


//        int x = 10;
//        IntStream.range(1, 2).boxed().forEach(i -> {
//            x = x + i;
//        });
    }

    //  обрабатываемую символьную строку
    static String stringOp(SomeFunc sf, String s) {
        return sf.func(s);
    }
}

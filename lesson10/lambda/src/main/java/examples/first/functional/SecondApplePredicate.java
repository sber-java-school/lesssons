package examples.first.functional;

import examples.Apple;

//@FunctionalInterface
public interface SecondApplePredicate {

    boolean test(Apple apple);

    boolean test2(Apple apple);

    boolean test3(Apple apple);

    boolean test4(Apple apple);

    boolean test5(Apple apple);
}

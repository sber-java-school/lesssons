package examples.first.functional;

import examples.Apple;

public interface ServiceInterface extends SecondApplePredicateImpl {

    boolean test(Apple apple);
}

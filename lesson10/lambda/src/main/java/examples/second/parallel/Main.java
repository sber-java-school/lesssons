package examples.second.parallel;

import java.util.Iterator;
import java.util.List;
import java.util.Spliterator;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Main {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        List<Integer> list = IntStream.range(1, 11).boxed().collect(Collectors.toList()); // 1 - 10
//        List<Integer> list = IntStream.rangeClosed(1, 11).boxed().collect(Collectors.toList()); // 1 - 11

//        list.stream().forEach(i -> {
//            System.out.println("Start: \tThread ID: " + Thread.currentThread().getId() + " : \tcount: " + i);
//            try {
//                Thread.sleep(2000);
//            } catch (InterruptedException ignore) {
//
//            }
//            System.out.println("Finish: \tThread ID: " + Thread.currentThread().getId() + " : \tcount: " + i);
//        });









//        list.parallelStream().forEach(i -> {
//            System.out.println("Start: \tThread ID: " + Thread.currentThread().getId() + " : \tcount: " + i);
//            try {
//                Thread.sleep(2000);
//            } catch (InterruptedException ignore) {
//
//            }
//            System.out.println("Finish: \tThread ID: " + Thread.currentThread().getId() + " : \tcount: " + i);
//        });

//        Spliterator<Integer> sp1 = list.spliterator();
//        sp1.forEachRemaining(System.out::println);
//
//        Spliterator<Integer> sp2 = sp1.trySplit();
//        System.out.println("sp1");
//        sp1.forEachRemaining(System.out::println);
//        System.out.println("sp2");
//        sp2.forEachRemaining(System.out::println);







        ForkJoinPool forkJoinPool = new ForkJoinPool(8);
        forkJoinPool.submit(
                () -> {
                    list.parallelStream().forEach(i -> {
                        System.out.println("Start: \tThread ID: " + Thread.currentThread().getId() + " : \tcount: " + i);
                        try {
                            Thread.sleep(2000);
                        } catch (InterruptedException ignore) {

                        }
                        System.out.println("Finish: \tThread ID: " + Thread.currentThread().getId() + " : \tcount: " + i);
                    });
                }
        ).get();
    }
}

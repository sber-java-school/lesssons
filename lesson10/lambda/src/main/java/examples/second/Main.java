package examples.second;

import examples.Apple;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

public class Main {

    public static void main(String[] args) {
        Apple green10 = new Apple("green", 10d);
        Apple green1000 = new Apple("green", 1000d);
        Apple red10 = new Apple("red", 10d);
        Apple red1000 = new Apple("red", 1000d);
        Apple red1000dubl = new Apple("red", 1000d);

        List<Apple> inventory = Arrays.asList(green10, green1000, red10, red1000, red1000dubl);


        // неявное использование equals() and hashcode()
        System.out.println(inventory.stream().distinct().collect(toList()).size());

        // map
        List<Double> map = inventory.stream()
                .map(apple -> apple.getWeight())
                .collect(toList());

        List<String> flatmap = inventory.stream()
                .map(Apple::getColor)
                .map(apple -> apple.split(""))
                .flatMap(apple -> Arrays.stream(apple))
                .collect(toList());

        inventory.stream()
                .flatMap(apple -> Arrays.stream(apple.getColor().split("")))
                .collect(toList());

        System.out.println(flatmap);

        // for each
        System.out.println("foreach");
        inventory.forEach(System.out::println);

        // peek
        System.out.println("peek");
        inventory.stream()
                .peek(apple -> apple.setWeight(apple.getWeight() * 2))
                .forEach(System.out::println);

        // max
        Optional<Apple> max = inventory.stream()
                .max(Comparator.comparingDouble(Apple::getWeight));

        // stat
        DoubleSummaryStatistics doubleSummaryStatistics = inventory.stream()
                .collect(Collectors.summarizingDouble(Apple::getWeight));

        doubleSummaryStatistics.getSum();
        doubleSummaryStatistics.getAverage();

        // group
        Map<String, List<Apple>> group = inventory.stream()
                .collect(Collectors.groupingBy(Apple::getColor));

        System.out.println("group");
        System.out.println(group);

        // partition
        Map<Boolean, List<Apple>> partition = inventory.stream()
                .collect(Collectors.partitioningBy(apple -> apple.getWeight() > 500));

        System.out.println("partition");
        System.out.println(partition);

        // toDouble
        double sum = inventory.stream().mapToDouble(Apple::getWeight).sum();

        // reduce
        Optional<Double> reduce =
                inventory.stream().map(Apple::getWeight).reduce(Double::sum);

        reduce.orElseThrow(RuntimeException::new);

        double v = reduce.isPresent() ? reduce.get() : 0d;
        double vv = reduce.orElse(0d);

        // optional
        List<Apple> list = new ArrayList<>();
        Apple apple = list.stream().findAny().orElse(new Apple("1", 1d));
//        Apple apple1 = list.stream().findAny().orElseThrow(RuntimeException::new);
//        Apple apple2 = list.stream().findAny().get(); // не проверено!

        // toMap
        inventory.stream().collect(Collectors.toMap(a -> a, Apple::getColor)); // duplicate key


        // limit
        List<Double> collect = inventory.stream()
                .map(Apple::getWeight)
                .limit(2).collect(toList());

        // skip
        inventory.stream().skip(2);

    }
}
